# Bookinfo Kubernetes

This repository contains all manifest files for deploying the [Bookinfo]() app into [Kubernetes]() cluster.

The following components will be deployed:

- [x] [mongo](https://gitlab.com/crowdarbookinfo/mongodb)
- [x] [ratings](https://gitlab.com/crowdarbookinfo/ratings)
- [x] [reviews](https://gitlab.com/crowdarbookinfo/reviews)
- [x] [details](https://gitlab.com/crowdarbookinfo/details)
- [x] [productpage](https://gitlab.com/crowdarbookinfo/productpage)


# Deploy the app

## Deploy the app with a single command
Run the following command for deploying the application:
```sh
make all
```


## Deploy the app step by step
```sh
kubectl apply -f mongodb/mongodb-namespace.yml
kubectl apply -f mongodb/mongodb-statefulset.yml
kubectl apply -f mongodb/mongodb-headless-service.yml

kubectl apply -f ratings/ratings-namespace.yml
kubectl apply -f ratings/ratings-configmap.yml
kubectl apply -f ratings/ratings-deployment.yml
kubectl apply -f ratings/ratings-service.yml

kubectl apply -f reviews/reviews-namespace.yml
kubectl apply -f reviews/reviews-configmap.yml
kubectl apply -f reviews/reviews-deployment.yml
kubectl apply -f reviews/reviews-service.yml

kubectl apply -f details/details-namespace.yml
kubectl apply -f details/details-configmap.yml
kubectl apply -f details/details-deployment.yml
kubectl apply -f details/details-service.yml

kubectl apply -f productpage/productpage-namespace.yml
kubectl apply -f productpage/productpage-configmap.yml
kubectl apply -f productpage/productpage-deployment.yml
kubectl apply -f productpage/productpage-service.yml
kubectl apply -f productpage/productpage-ingress.yml
```

## More...
> **Do you want to know more about this repo?**
> 
> This repo was created as part of a challenge developed by [Crowdar Academy](), and the details about it can be found in [Challenge-k8s](https://gitlab.com/crowdarbookinfo/challenge-k8s).