all: setup clean deploy

setup:
	minikube start
	minikube addons enable ingress

clean:
	kubectl delete ns/mongodb-ns
	kubectl delete ns/ratings-ns
	kubectl delete ns/reviews-ns
	kubectl delete ns/details-ns
	kubectl delete ns/productpage-ns
	
mongo:
	kubectl apply -f mongodb/mongodb-namespace.yml
	kubectl apply -f mongodb/mongodb-statefulset.yml
	kubectl apply -f mongodb/mongodb-headless-service.yml

rating:
	kubectl apply -f ratings/ratings-namespace.yml
	kubectl apply -f ratings/ratings-configmap.yml
	kubectl apply -f ratings/ratings-deployment.yml
	kubectl apply -f ratings/ratings-service.yml

review:
	kubectl apply -f reviews/reviews-namespace.yml
	kubectl apply -f reviews/reviews-configmap.yml
	kubectl apply -f reviews/reviews-deployment.yml
	kubectl apply -f reviews/reviews-service.yml

detail:
	kubectl apply -f details/details-namespace.yml
	kubectl apply -f details/details-configmap.yml
	kubectl apply -f details/details-deployment.yml
	kubectl apply -f details/details-service.yml

product:
	kubectl apply -f productpage/productpage-namespace.yml
	kubectl apply -f productpage/productpage-configmap.yml
	kubectl apply -f productpage/productpage-deployment.yml
	kubectl apply -f productpage/productpage-service.yml
	kubectl apply -f productpage/productpage-ingress.yml

deploy: mongo rating review detail product
